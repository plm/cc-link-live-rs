use super::*;

use core::mem;
use fugit::MicrosDuration;
use heapless::Vec;

type Duration = MicrosDuration<u32>;
type Values = Vec<Duration, 10>;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
struct Calibration(Duration);

#[derive(Debug, PartialEq, Eq, Default)]
enum State {
    #[default]
    Reset,
    Calibration(Calibration),
    Data(Calibration, Values),
}

#[derive(Debug, PartialEq, Eq)]
pub struct Needed(usize);

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    InvalidState,
    IncompleteData(Needed),
}

#[cfg(feature = "std")]
impl std::error::Error for Error {}

#[derive(Debug, PartialEq, Eq, Default)]
pub struct TelemetryBuilder(State);

impl TelemetryBuilder {
    pub const fn new() -> Self {
        Self(State::Reset)
    }

    pub fn reset(&mut self) {
        self.0 = State::Reset
    }

    pub fn is_reset(&self) -> bool {
        self.0 == State::Reset
    }

    pub fn record_pulse(&mut self, duration: Duration) -> Result<(), Duration> {
        let state = &mut self.0;
        match state {
            State::Data(_, ref mut values) => values.push(duration),
            State::Calibration(ref calibration) => {
                let mut values = Values::new();
                values.push(duration)?;
                let _ = mem::replace(state, State::Data(*calibration, values));
                Ok(())
            }
            State::Reset => {
                let _ = mem::replace(state, State::Calibration(Calibration(duration)));
                Ok(())
            }
        }
    }

    pub fn build(&mut self) -> Result<Telemetry, Error> {
        let state = &mut self.0;
        match state {
            State::Data(Calibration(ref calib_1000), ref values) => {
                if let [voltage, ripple_voltage, current, throttle, output_power, rpm, bec_voltage, bec_current, maybe_linear_temp, maybe_ntc_temp] =
                    &values[..]
                {
                    let (is_linear, temp, calib_500) = if maybe_linear_temp > maybe_ntc_temp {
                        (true, maybe_linear_temp, maybe_ntc_temp)
                    } else {
                        (false, maybe_ntc_temp, maybe_linear_temp)
                    };
                    let calibrate = |duration: &Duration| {
                        (duration.max(calib_500).to_micros() - calib_500.to_micros()) as f32
                            / calib_1000.to_micros() as f32
                    };
                    let calibrate_opt = |duration: &Duration| {
                        let result = calibrate(duration);
                        if result == 0.0 {
                            None
                        } else {
                            Some(result)
                        }
                    };
                    let temp = calibrate(temp);
                    let temp = if is_linear {
                        Temperature::Linear(Scaled(temp))
                    } else {
                        Temperature::NTC(ScaledDecimal(temp))
                    };
                    let telemetry = Telemetry::new(
                        Voltage(calibrate(voltage).into()),
                        RippleVoltage(calibrate(ripple_voltage).into()),
                        calibrate_opt(current).map(|c| Current(c.into())),
                        Throttle(calibrate(throttle)),
                        OutputPower(calibrate(output_power).into()),
                        RPM(calibrate(rpm).into()),
                        calibrate_opt(bec_voltage).map(|bv| BecVoltage(bv.into())),
                        calibrate_opt(bec_current).map(|bc| BecCurrent(bc.into())),
                        temp,
                    );
                    let _ = mem::replace(state, State::Reset);
                    Ok(telemetry)
                } else {
                    Err(Error::IncompleteData(Needed(
                        values.capacity() - values.len(),
                    )))
                }
            }
            _ => Err(Error::InvalidState),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn initial_builder_state() {
        for b in [TelemetryBuilder::new(), TelemetryBuilder::default()] {
            assert_eq!(b.0, State::Reset)
        }
    }

    #[test]
    fn build_telemetry() {
        let mut tb = TelemetryBuilder::new();
        let calibration = Duration::micros(998);
        assert_eq!(tb.record_pulse(calibration), Ok(()));
        let calibration = Calibration(calibration);
        assert_eq!(tb.0, State::Calibration(calibration));
        assert_eq!(tb.build(), Err(Error::InvalidState));
        for duration in [1077, 499, 499, 1992, 499, 499, 499, 499, 1613, 499] {
            assert!(tb.build().is_err());
            let duration = Duration::micros(duration);
            assert_eq!(tb.record_pulse(duration), Ok(()));
            if let State::Data(calib, ref values) = tb.0 {
                assert_eq!(calib, calibration);
                assert_eq!(values.last(), Some(&duration));
            } else {
                unreachable!();
            }
        }
        if let Ok(t) = tb.build() {
            assert_eq!(f32::from(t.voltage), 11.583166);
            assert_eq!(f32::from(t.ripple_voltage), 0.0);
            assert_eq!(t.current, None);
            assert_eq!(f32::from(t.throttle), 1.495992);
            assert_eq!(f32::from(t.output_power), 0.0);
            assert_eq!(f32::from(t.rpm), 0.0);
            assert_eq!(t.bec_voltage, None);
            assert_eq!(t.bec_current, None);
            assert_eq!(f32::from(t.temperature), 33.486977);
        } else {
            unreachable!()
        }
        assert_eq!(tb.0, State::Reset);
    }
}
