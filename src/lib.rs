#![no_std]

pub mod protocol;

#[cfg(test)]
mod tests {

    #[test]
    #[should_panic]
    fn canary() {
        panic!();
    }
}
