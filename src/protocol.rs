#[allow(unused_imports)]
use micromath::F32Ext;

pub mod builder;

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Scaled<const SCALE: u32>(f32);

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct ScaledDecimal<const SCALE: u32, const DIVISOR: u32>(f32);

impl<const SCALE: u32> Scaled<SCALE> {
    fn scaled(&self) -> f32 {
        self.0 * SCALE as f32
    }
}

impl<const SCALE: u32> From<f32> for Scaled<SCALE> {
    fn from(value: f32) -> Self {
        Self(value)
    }
}

impl<const SCALE: u32, const DIVISOR: u32> ScaledDecimal<SCALE, DIVISOR> {
    fn scaled(&self) -> f32 {
        self.0 * SCALE as f32 / DIVISOR as f32
    }
}

impl<const SCALE: u32, const DIVISOR: u32> From<f32> for ScaledDecimal<SCALE, DIVISOR> {
    fn from(value: f32) -> Self {
        Self(value)
    }
}

macro_rules! scaled_impl {
    ( $t:ty ) => {
        impl From<$t> for f32 {
            fn from(t: $t) -> Self {
                t.0.scaled()
            }
        }

        impl From<f32> for $t {
            fn from(value: f32) -> Self {
                Self(value.into())
            }
        }
    };
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Voltage(pub Scaled<20>);

scaled_impl! { Voltage }

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct RippleVoltage(pub Scaled<4>);

scaled_impl! { RippleVoltage }

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Current(pub Scaled<50>);

scaled_impl! { Current }

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Throttle(pub f32);

impl From<Throttle> for f32 {
    fn from(throttle: Throttle) -> Self {
        throttle.0
    }
}

impl From<f32> for Throttle {
    fn from(value: f32) -> Self {
        Self(value)
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct OutputPower(pub ScaledDecimal<2_502, 10_000>);

scaled_impl! { OutputPower }

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct RPM(pub ScaledDecimal<204_167, 10>);

scaled_impl! { RPM }

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct BecVoltage(pub Scaled<4>);

scaled_impl! { BecVoltage }

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct BecCurrent(pub Scaled<4>);

scaled_impl! { BecCurrent }

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Temperature {
    Linear(Scaled<30>),
    NTC(ScaledDecimal<638_125, 10_000>),
}

const R0: f32 = 10_000.0;
const R2: f32 = 10_200.0;
const B: f32 = 3_455.0;

impl From<Temperature> for f32 {
    fn from(temperature: Temperature) -> Self {
        match temperature {
            Temperature::Linear(value) => value.scaled(),
            Temperature::NTC(value) => {
                let value: Self = value.scaled();
                1.0 / ((value * R2 / (255.0 - value) / R0).ln() / B + 1.0 / 298.0) - 273.0
            }
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct Telemetry {
    pub voltage: Voltage,
    pub ripple_voltage: RippleVoltage,
    pub current: Option<Current>,
    pub throttle: Throttle,
    pub output_power: OutputPower,
    pub rpm: RPM,
    pub bec_voltage: Option<BecVoltage>,
    pub bec_current: Option<BecCurrent>,
    pub temperature: Temperature,
}

impl Telemetry {
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        voltage: Voltage,
        ripple_voltage: RippleVoltage,
        current: Option<Current>,
        throttle: Throttle,
        output_power: OutputPower,
        rpm: RPM,
        bec_voltage: Option<BecVoltage>,
        bec_current: Option<BecCurrent>,
        temperature: Temperature,
    ) -> Self {
        Self {
            voltage,
            ripple_voltage,
            current,
            throttle,
            output_power,
            rpm,
            bec_voltage,
            bec_current,
            temperature,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn scaled_conversion() {
        assert_eq!(Scaled::<10>::from(1.0).scaled(), 10.0);
    }

    #[test]
    fn scaled_decimal_conversion() {
        assert_eq!(ScaledDecimal::<5, 10>::from(1.0).scaled(), 0.5);
    }

    #[test]
    fn voltage_conversion() {
        assert_eq!(f32::from(Voltage(Scaled(1.0))), 20.0);
    }

    #[test]
    fn ripple_voltage_conversion() {
        assert_eq!(f32::from(RippleVoltage(Scaled(1.0))), 4.0);
    }

    #[test]
    fn current_conversion() {
        assert_eq!(f32::from(Current(Scaled(1.0))), 50.0);
    }

    #[test]
    fn throttle_conversion() {
        assert_eq!(f32::from(Throttle(1.0)), 1.0);
    }

    #[test]
    fn output_power_conversion() {
        assert_eq!(f32::from(OutputPower(ScaledDecimal(1.0))), 0.2502);
    }

    #[test]
    fn rpm_conversion() {
        assert_eq!(f32::from(RPM(ScaledDecimal(1.0))), 20416.7);
    }

    #[test]
    fn bec_voltage_conversion() {
        assert_eq!(f32::from(BecVoltage(Scaled(1.0))), 4.0);
    }

    #[test]
    fn bec_current_conversion() {
        assert_eq!(f32::from(BecCurrent(Scaled(1.0))), 4.0);
    }

    #[test]
    fn temperature_conversion() {
        assert_eq!(f32::from(Temperature::Linear((3.0).into())), 90.0);
        assert_eq!(f32::from(Temperature::NTC((0.43).into())), 90.74835);
    }
}
